// Create the 'articles' service
angular.module('index').factory('Rulesets', ['$resource', function($resource) {
	// Use the '$resource' service to return a ruleset '$resource' object
    return $resource('api/rulesets/:rulesetId', {rulesetId: '@id'}, { 
    	delete: { method: 'DELETE' },
    	update: { method: 'PUT' }
    });
}]);