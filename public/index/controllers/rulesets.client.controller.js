// Create the 'rulesets' controller
angular.module('index').controller('RulesetController', ['$scope', '$routeParams', '$timeout', '$location', '$anchorScroll', 'Rulesets', 'Solstice',
    function($scope, $routeParams, $timeout, $location, $anchorScroll, Rulesets, Solstice) {

        // Initialzing the rulesets
        $scope.search_initiated = false;
        $scope.existingRulesetList = Rulesets.query();
        $scope.disableExistingRuleset = true;
        $scope.useExistingRuleset = false;
        $scope.editing = false;
        $scope.existingRulesetTitle = null;
        $scope.disclaimer = true;
        $scope.noRuleset = true;
        $scope.rulesetTitle = null;
        $scope.review = false;
        $scope.hiddenDivContainer = false;

        $scope.rulesetCriteriaDropdownOptions = [];
        $scope.runNumberSelections = [];
        $scope.assetTypeSelections = [];
        $scope.sellerCodeSelections = [];
        $scope.statusSelections = [];
        $scope.daysToAuctionSelections = [];
        $scope.startingBidSelections = [];
        $scope.zoltarScoreSelections = [];

        var rulesetArray = [];
        var prevID = undefined;
        var sl = $scope.selectedOption;
        var boostBy = "";

        $scope.selected = undefined;
        $scope.states = [{'name': 'Alabama', 'abbr': 'AL', 'lat': 32, 'long': -86}, {'name': 'Alaska', 'abbr': 'AK', 'lat': 64, 'long': -149}, {'name':'Arizona', 'abbr': 'AZ', 'lat': 34, 'long': -111}, {'name': 'Arkansas', 'abbr': 'AR', 'lat': 35, 'long': -91}, {'name': 'California', 'abbr': 'CA', 'lat': 36, 'long': -119}, {'name': 'Colorado', 'abbr': 'CO', 'lat': 39, 'long': -105}, {'name': 'Connecticut', 'abbr': 'CT', 'lat': 41, 'long': -73}, {'name': 'Delaware', 'abbr': 'DE', 'lat': 38, 'long': -75}, {'name': 'Florida', 'abbr': 'FL', 'lat': 27, 'long': -81}, {'name': 'Georgia', 'abbr': 'GA', 'lat': 32, 'long': -82}, {'name': 'Hawaii', 'abbr': 'HI', 'lat': 19, 'long': -155}, {'name': 'Idaho', 'abbr': 'ID', 'lat': 44, 'long': -114}, {'name': 'Illinois', 'abbr': 'IL', 'lat': 40, 'long': -89}, {'name': 'Indiana', 'abbr': 'IN', 'lat': 40, 'long': -86}, {'name': 'Iowa', 'abbr': 'IA', 'lat': 41, 'long': -93}, {'name': 'Kansas', 'abbr': 'KS', 'lat': 39, 'long': -98}, {'name': 'Kentucky', 'abbr': 'KY', 'lat': 37, 'long': -84}, {'name': 'Louisiana', 'abbr': 'LA', 'lat': 30, 'long': -91}, {'name': 'Maine', 'abbr': 'ME', 'lat': 45, 'long': -69}, {'name': 'Maryland', 'abbr': 'MD', 'lat': 39, 'long': -76}, {'name': 'Massachusetts', 'abbr': 'MA', 'lat': 42, 'long': -71},{'name': 'Michigan', 'abbr': 'MI', 'lat': 44, 'long': -85}, {'name': 'Minnesota', 'abbr': 'MN', 'lat': 46, 'long': -94}, {'name': 'Mississippi', 'abbr': 'MS', 'lat': 32, 'long': -89}, {'name': 'Missouri', 'abbr': 'MO', 'lat': 37, 'long': -91}, {'name': 'Montana', 'abbr': 'MT', 'lat': 46, 'long': -110}, {'name': 'Nebraska', 'abbr': 'NE', 'lat': 41, 'long': -99}, {'name': 'Nevada', 'abbr': 'NV', 'lat': 38, 'long': -116}, {'name': 'New Hampshire', 'abbr': 'NH', 'lat': 43, 'long': -71}, {'name': 'New Jersey', 'abbr': 'NJ', 'lat': 40, 'long': -74}, {'name': 'New Mexico', 'abbr': 'NM', 'lat': 34, 'long': -105}, {'name': 'New York', 'abbr': 'NY', 'lat': 34, 'long': -105}, {'name': 'North Dakota', 'abbr': 'ND', 'lat': 47, 'long': -101}, {'name': 'North Carolina', 'abbr': 'NC', 'lat': 35, 'long': -79}, {'name': 'Ohio', 'abbr': 'OH', 'lat': 40, 'long': -82}, {'name': 'Oklahoma', 'abbr': 'OK', 'lat': 35, 'long': -97}, {'name': 'Oregon', 'abbr': 'OR', 'lat': 43, 'long': -120}, {'name': 'Pennsylvania', 'abbr': 'PA', 'lat': 41, 'long': -77}, {'name': 'Rhode Island', 'abbr': 'RI', 'lat': 41, 'long': -71}, {'name': 'South Carolina', 'abbr': 'SC', 'lat': 33, 'long': -81}, {'name': 'South Dakota', 'abbr': 'SD', 'lat': 43, 'long': -99}, {'name': 'Tennessee', 'abbr': 'TN', 'lat': 35, 'long': -86}, {'name': 'Texas', 'abbr': 'TX', 'lat': 31, 'long': -99}, {'name': 'Utah', 'abbr': 'UT', 'lat': 39, 'long': -111}, {'name': 'Vermont', 'abbr': 'VT', 'lat': 44, 'long': -72}, {'name': 'Virgina', 'abbr': 'VA', 'lat': 37, 'long': -78}, {'name': 'Washington', 'abbr': 'WA', 'lat': 47, 'long': -120}, {'name': 'West Virginia', 'abbr': 'FL', 'lat': 38, 'long': -80}, {'name': 'Wisconsin', 'abbr': 'WI', 'lat': 43, 'long': -88}, {'name': 'Wyoming', 'abbr': 'WY', 'lat': 43, 'long': -107}];
        $scope.alerts = [];

        $scope.addSuccessfulAlert = function(option) {
            if (option == 1)
                $scope.alerts.push({type: 'success', msg: 'You have successfully saved a ruleset'});
            else if (option == 2)
                $scope.alerts.push({type: 'success', msg: 'You have successfully deleted a ruleset'});
            else if (option == 3)
                $scope.alerts.push({type: 'success', msg: 'You have successfully updated a ruleset'});
            $timeout(function() {
                $scope.alerts.splice($scope.alerts.indexOf(option), 1);
            }, 3000);
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.sortCriteriaOptions = [
            {"value": "asset_type", "text": "Asset Type"},
            {"value": "run_number", "text": "Run Number"},
            {"value": "seller_code", "text": "Seller Code"},
            {"value": "status", "text": "Status"},
            {"value": "days_to_auction", "text": "Days To Auction"},
            {"value": "starting_bid", "text": "Starting Bid"},
            {"value": "zoltar_score", "text": "Zoltar Score"}
        ];

        $scope.runNumberOptions = [
            {"value": "less_than", "text": "Less Than"},
            {"value": "greater_than", "text": "Greater Than"},
            {"value": "between", "text": "Between"}
        ];

        $scope.sellerCodeOptions = [
            {"value": "211", "text": "211"},
            {"value": "3ml", "text": "3ML"},
            {"value": "400", "text": "400"},
            {"value": "505", "text": "505"},
            {"value": "639", "text": "639"},
            {"value": "aaa", "text": "AAA"}
        ];

        $scope.assetTypeOptions = [
            {"value": "bank_owned", "text": "Bank Owned"},
            {"value": "foreclosure", "text": "Foreclosure"},
            {"value": "income_producing", "text": "Income Producing"},
            {"value": "private_seller", "text": "Private Seller"},
            {"value": "short_sell", "text": "Short Sell"}
        ];

        $scope.statusTypeOptions = [
            {"value": "auction", "text": "Auction"},
            {"value": "canceled", "text": "Canceled"},
            {"value": "postponed", "text": "Postponed"},
            {"value": "sold_presale", "text": "Sold Presale"}
        ];
        //Need to modify
        $scope.daysToAuctionOptions = [
            {"value": "less_than", "text": "Less Than"},
            {"value": "greater_than", "text": "Greater Than"},
            {"value": "between", "text": "Between"}
        ];

        $scope.startingBidOptions = [
            {"value": "less_than", "text": "Less Than"},
            {"value": "greater_than", "text": "Greater Than"},
            {"value": "between", "text": "Between"}
        ];

        $scope.zoltarScoreOptions = [
            {"value": "less_than", "text": "Less Than"},
            {"value": "greater_than", "text": "Greater Than"},
            {"value": "between", "text": "Between"}
        ];

        $scope.selectSection = function(sec) {
            //console.log("selectSection " + sec);
            $scope.selectSectionParent(sec);
        }

        $scope.isSelected = function(sec) {
            return $scope.isSelectedParent(sec);
        }

        $scope.toggleHiddenContainer = function() {
            if ($scope.hiddenDivContainer == false)
                $scope.hiddenDivContainer = true;
        };

        $scope.rulesetExist = function() {
            if ($scope.rulesetCriteriaDropdownOptions.length > 0)
                return true;
            else
                return false;
        };

        $scope.create = function() {
            // Use the form fields to create a new ruleset $resource object
            var ruleset = new Rulesets({
                title: $scope.rulesetTitle,
                rulesetArray: rulesetArray
            }); 

            // Use the ruleset '$save' method to send an appropriate POST request
            ruleset.$save(function(response) {
                console.log("Successful!");
                $scope.selectSectionParent(5);
                $scope.disableExistingRuleset = true;
                $scope.editing = true;
                $scope.cancel();
            }, function(errorResponse) { // Otherwise, present the user with the error message
                    console.log("Failed! This is the response: " + errorResponse);
                $scope.error = errorResponse.data.message;
            });
        };

        // Create a new controller method for retrieving a list of rulesets
        $scope.findExistingRulesets = function() {
        	// Use the ruleset 'query' method to send an appropriate GET request
            if ($scope.section1 == false) {
                console.log("Not opened")
                $scope.existingRulesetList = Rulesets.query();

                Rulesets.query(function (data) {
                    console.log(data);
                });
            }
        };

        $scope.editExistingRuleset = function() { 
            $scope.disableExistingRuleset = true;
            $scope.editing = true;
        };

        $scope.cancelEditing = function() {
            $scope.disableExistingRuleset = "editExistingRuleset";
            $scope.editing = false;
        };

        $scope.disableRulesetContainers = function() {
            $scope.disableExistingRuleset = "editExistingRuleset";
        };

        $scope.enableRulesetContainers = function() {
            $scope.disableExistingRuleset = true;
        };


        // Create a new controller method for updating a single ruleset
        $scope.update = function() {
            console.log("Inside of update!: " + $scope.rulesetID);
            // Use the ruleset '$update' method to send an appropriate PUT request
            var ruleset = Rulesets.get({ rulesetId: $scope.rulesetID });
            // console.log(ruleset);
            // console.log($scope.rulesetTitle);
            // console.log("Old title: " + ruleset.title);
            ruleset.title = $scope.rulesetTitle;
            Rulesets.update({ rulesetId: $scope.rulesetID }, ruleset);
            $scope.selectSectionParent(5);
            $scope.disableExistingRuleset = false;
            $scope.editing = true;
            $scope.useExistingRuleset = false;
            $scope.cancel();
        };

        // Create a new controller method for retrieving a single ruleset
        $scope.findOne = function() {
            $scope.disableExistingRuleset = "editExistingRuleset";
            $scope.useExistingRuleset = true;
            if (checkRulesetChange() == true) /* This checks to see if the same ruleset is clicked consecutively */
                return;
            console.log($scope.rulesetID);
            var promise = Rulesets.get({
                rulesetId: $scope.rulesetID
            }).$promise.then(function (returnedRuleset) {
                for (var i = 0; i< returnedRuleset.rulesetArray.length; i++) {
                    var criteriaType = Object.keys(returnedRuleset.rulesetArray[i]);
                    var obj = returnedRuleset.rulesetArray[i];
                    $scope.rulesetCriteriaDropdownOptions.push({'type': criteriaType[0]});
                    for (var x in obj) {
                        if (obj.hasOwnProperty(x)) {
                            var obj2 = obj[x];
                            for (var y in obj2) {
                                if (obj2.hasOwnProperty(y)) {
                                    console.log("here: " + obj2[y].selection);
                                    loadTypeSelections(criteriaType[0], obj2[y]);
                                }
                            }
                        }
                    }
                }
                console.log(JSON.stringify(returnedRuleset));
                $scope.existingRulesetTitle = returnedRuleset.title;
            });
        };

        var loadTypeSelections = function(criteriaType, obj) {
            if (criteriaType == 'asset_type') {
                $scope.assetTypeSelections.push({'selection': obj.selection});
            } else if (criteriaType == 'run_number') {
                if (obj.selection == 'less_than') {
                    $scope.runNumberSelections.push({'selection': obj.selection, 'less_than_value': obj.less_than_value});
                    boostBy+="Run_Number=";
                    boostBy+=obj.less_than_value;
                } else if (obj.selection == 'greater_than') {
                    $scope.runNumberSelections.push({'selection': obj.selection, 'greater_than_value': obj.greater_than_value});
                } else if (obj.selection == 'between') {
                    $scope.runNumberSelections.push({'selection': obj.selection, 'between1_value': obj.between1_value, 'between2_value': obj.between2_value});
                }
            } else if (criteriaType == 'seller_code') {
                $scope.sellerCodeSelections.push({'selection': obj.selection});
            } else if (criteriaType == 'status') {
                $scope.statusSelections.push({'selection': obj.selection});
            } else if (criteriaType == 'days_to_auction') {
                if (obj.selection == 'less_than') 
                    $scope.daysToAuctionSelections.push({'selection': obj.selection, 'less_than_value': obj.less_than_value});
                else if (obj.selection == 'greater_than')
                    $scope.daysToAuctionSelections.push({'selection': obj.selection, 'greater_than_value': obj.greater_than_value});
                else if (obj.selection == 'between')
                    $scope.daysToAuctionSelections.push({'selection': obj.selection, 'between1_value': obj.between1_value, 'between2_value': obj.between2_value});
            } else if (criteriaType == 'starting_bid') {
                if (obj.selection == 'less_than') 
                    $scope.startingBidSelections.push({'selection': obj.selection, 'less_than_value': obj.less_than_value});
                else if (obj.selection == 'greater_than')
                    $scope.startingBidSelections.push({'selection': obj.selection, 'greater_than_value': obj.greater_than_value});
                else if (obj.selection == 'between')
                    $scope.startingBidSelections.push({'selection': obj.selection, 'between1_value': obj.between1_value, 'between2_value': obj.between2_value});
            } else if (criteriaType == 'zoltar_score') {
                if (obj.selection == 'less_than') 
                    $scope.zoltarScoreSelections.push({'selection': obj.selection, 'less_than_value': obj.less_than_value});
                else if (obj.selection == 'greater_than')
                    $scope.zoltarScoreSelections.push({'selection': obj.selection, 'greater_than_value': obj.greater_than_value});
                else if (obj.selection == 'between')
                    $scope.zoltarScoreSelections.push({'selection': obj.selection, 'between1_value': obj.between1_value, 'between2_value': obj.between2_value});
            }
        }
        // 
        /* Function:    checkRulesetChange
         * Description: This method checks to see the change of rulesets.
         */
        var checkRulesetChange = function() {
            if ($scope.rulesetID[0] == prevID) {
                // console.log("Trying to load same ruleset");
                return true;
            }
            if (prevID == null) {
                // console.log("prevID is null");
                prevID = $scope.rulesetID[0];
                // console.log("Assigned prevID " + $scope.rulesetID[0]);
            }
            if ($scope.rulesetID[0] != prevID) {
                // console.log("Loading different ruleset");
                prevID = $scope.rulesetID[0];
                $scope.cancel();
            }
        }

        $scope.deleteRuleset = function () {
            console.log("Inside of delete!: " + $scope.rulesetID);
            Rulesets.delete({ rulesetId: $scope.rulesetID });
            for (var i in $scope.existingRulesetList) {
                if ($scope.existingRulesetList[i]._id == $scope.rulesetID) {
                    $scope.existingRulesetList.splice(i, 1);
                }
            }
            console.log("Delete Successful!");
            $scope.cancel();
            $scope.useExistingRuleset = false;
            $scope.disableExistingRuleset = true;
            $scope.selectSection(4);
            $scope.selectSection(1);
        };

        $scope.cancel = function() {
            $scope.hiddenDivContainer = false;
            $scope.sortCriteriaOptions = [
                {"value": "asset_type", "text": "Asset Type"},
                {"value": "run_number", "text": "Run Number"},
                {"value": "seller_code", "text": "Seller Code"},
                {"value": "status", "text": "Status"},
                {"value": "days_to_auction", "text": "Days To Auction"},
                {"value": "starting_bid", "text": "Starting Bid"},
                {"value": "zoltar_score", "text": "Zoltar Score"}
            ];
            $scope.rulesetCriteriaDropdownOptions = [];
            $scope.runNumberSelections = [];
            $scope.assetTypeSelections = [];
            $scope.sellerCodeSelections = [];
            $scope.statusSelections = [];
            $scope.daysToAuctionOptionsArray = [];
            $scope.startingBidSelections = [];
            $scope.zoltarScoreSelections = [];
        };

        $scope.removeOption = function(array, index) {
            if (array[index].selection != 'undefined') {
                console.log(array[index].selection);
                array.splice(index, 1);
            }
        };

        $scope.removeCriteriaOption = function(array, option, index) {
            if (option == 'asset_type') {
                $scope.sortCriteriaOptions.push({"value": "asset_type", "text": "Asset Type"});
                array.splice(index, 1);
            } else if (option == 'run_number') {
                $scope.sortCriteriaOptions.push({"value": "run_number", "text": "Run Number"});
                array.splice(index, 1);
            } else if (option == 'seller_code') {
                $scope.sortCriteriaOptions.push({"value": "seller_code", "text": "Seller Code"});
                array.splice(index, 1);
            } else if (option == 'status') {
                $scope.sortCriteriaOptions.push({"value": "status", "text": "Status"});
                array.splice(index, 1);
            } else if (option == 'days_to_auction') {
                $scope.sortCriteriaOptions.push({"value": "days_to_auction", "text": "Days To Auction"});
                array.splice(index, 1);
            } else if (option == 'starting_bid') {
                $scope.sortCriteriaOptions.push({"value": "starting_bid", "text": "Starting Bid"});
                array.splice(index, 1);
            } else if (option == 'zoltar_score') {
                $scope.sortCriteriaOptions.push({"value": "zoltar_score", "text": "Zoltar Score"});
                array.splice(index, 1);
            }
            if (array.length == 0) {
                $scope.hiddenDivContainer = false;
            }
        };

        $scope.remove = function(criteria, array, option, index) {
            console.log(array);
            if (criteria == 'asset_type') {
                if (option == 'bank_owned') {
                    $scope.assetTypeOptions.push({"value": "bank_owned", "text": "Bank Owned"});
                    array.splice(index, 1);
                } else if (option == 'foreclosure') {
                    $scope.assetTypeOptions.push({"value": "foreclosure", "text": "Foreclosure"});
                    array.splice(index, 1);
                } else if (option == 'income_producing') {
                    $scope.assetTypeOptions.push({"value": "income_producing", "text": "Income Producing"});
                    array.splice(index, 1);
                } else if (option == 'private_seller') {
                    $scope.assetTypeOptions.push({"value": "private_seller", "text": "Private Seller"});
                    array.splice(index, 1);
                } else if (option == 'short_sell') {
                    $scope.assetTypeOptions.push({"value": "short_sell", "text": "Short Sell"});
                    array.splice(index, 1);
                }
            } else if (criteria == 'run_number') {
                if (option == 'less_than') {
                    $scope.runNumberOptions.push({"value": "less_than", "text": "Less Than"});
                    array.splice(index, 1);
                } else if (option == 'greater_than') {
                    $scope.runNumberOptions.push({"value": "greater_than", "text": "Greater Than"});
                    array.splice(index, 1);
                } else if (option == 'between') {
                    $scope.runNumberOptions.push({"value": "between", "text": "Between"});
                    array.splice(index, 1);
                }
            } else if (criteria == 'seller_code') {
                if (option == '211') {
                    $scope.sellerCodeOptions.push({"value": "211", "text": "211"});
                    array.splice(index, 1);
                } else if (option == '3ml') {
                    $scope.sellerCodeOptions.push({"value": "3ml", "text": "3ML"});
                    array.splice(index, 1);
                } else if (option == '400') {
                    $scope.sellerCodeOptions.push({"value": "400", "text": "400"});
                    array.splice(index, 1);
                } else if (option == '505') {
                    $scope.sellerCodeOptions.push({"value": "505", "text": "505"});
                    array.splice(index, 1);
                } else if (option == '639') {
                    $scope.sellerCodeOptions.push({"value": "639", "text": "639"});
                    array.splice(index, 1);
                } else if (option == 'aaa') {
                    $scope.sellerCodeOptions.push({"value": "aaa", "text": "AAA"});
                    array.splice(index, 1);
                }
            } else if (criteria == 'status') {
                if (option == 'auction') {
                    $scope.statusTypeOptions.push({"value": "auction", "text": "Auction"});
                    array.splice(index, 1);
                } else if (option == 'canceled') {
                    $scope.statusTypeOptions.push({"value": "canceled", "text": "Canceled"});
                    array.splice(index, 1);
                } else if (option == 'postponed') {
                    $scope.statusTypeOptions.push({"value": "postponed", "text": "Postponed"});
                    array.splice(index, 1);
                } else if (option == 'sold_presale') {
                    $scope.statusTypeOptions.push({"value": "sold_presale", "text": "Sold Presale"});
                    array.splice(index, 1);
                } else if (criteria == 'status') {

                }
            } else if (criteria == 'days_to_auction') {
                if (option == 'less_than') {
                    $scope.daysToAuctionOptions.push({"value": "less_than", "text": "Less Than"});
                    array.splice(index, 1);
                } else if (option == 'greater_than') {
                    $scope.daysToAuctionOptions.push({"value": "greater_than", "text": "Greater Than"});
                    array.splice(index, 1);
                } else if (option == 'between') {
                    $scope.daysToAuctionOptions.push({"value": "between", "text": "Between"});
                    array.splice(index, 1);
                }
            } else if (criteria == 'starting_bid') {
                if (option == 'less_than') {
                    $scope.startingBidOptions.push({"value": "less_than", "text": "Less Than"});
                    array.splice(index, 1);
                } else if (option == 'greater_than') {
                    $scope.startingBidOptions.push({"value": "greater_than", "text": "Greater Than"});
                    array.splice(index, 1);
                } else if (option == 'between') {
                    $scope.startingBidOptions.push({"value": "between", "text": "Between"});
                    array.splice(index, 1);
                }
            } else if (criteria == 'zoltar_score') {
                if (option == 'less_than') {
                    $scope.zoltarScoreOptions.push({"value": "less_than", "text": "Less Than"});
                    array.splice(index, 1);
                } else if (option == 'greater_than') {
                    $scope.zoltarScoreOptions.push({"value": "greater_than", "text": "Greater Than"});
                    array.splice(index, 1);
                } else if (option == 'between') {
                    $scope.zoltarScoreOptions.push({"value": "between", "text": "Between"});
                    array.splice(index, 1);
                }
            }
        };

        $scope.addCriteriaOption = function(option) {
            console.log('Value = ' + option.value);
            if (option.value == 'asset_type') {
                $scope.rulesetCriteriaDropdownOptions.push({'type': 'asset_type'});
                rulesetArray.push({'asset_type': $scope.assetTypeSelections});
            } else if (option.value == 'run_number') {
                $scope.rulesetCriteriaDropdownOptions.push({'type': 'run_number'});
                rulesetArray.push({'run_number': $scope.runNumberSelections});
            } else if (option.value == 'seller_code') {
                $scope.rulesetCriteriaDropdownOptions.push({'type': 'seller_code'});
                rulesetArray.push({'seller_code': $scope.sellerCodeSelections});
            } else if (option.value == 'status') {
                $scope.rulesetCriteriaDropdownOptions.push({'type': 'status'});
                rulesetArray.push({'status': $scope.statusSelections});
            } else if (option.value == 'days_to_auction') {
                $scope.rulesetCriteriaDropdownOptions.push({'type': 'days_to_auction'});
                rulesetArray.push({'days_to_auction': $scope.daysToAuctionSelections});
            } else if (option.value == 'starting_bid') {
                $scope.rulesetCriteriaDropdownOptions.push({'type': 'starting_bid'});
                rulesetArray.push({'starting_bid': $scope.startingBidSelections});
            } else if (option.value == 'zoltar_score') {
                $scope.rulesetCriteriaDropdownOptions.push({'type': 'zoltar_score'});
                rulesetArray.push({'zoltar_score': $scope.zoltarScoreSelections});
            }
        };

        $scope.addAssetTypeOption = function(option) {
            if (option.value == 'bank_owned') {
                $scope.assetTypeSelections.push({'selection': 'bank_owned'});
            } else if (option.value == 'foreclosure') {
                $scope.assetTypeSelections.push({'selection': 'foreclosure'});
            } else if (option.value == 'income_producing') {
                $scope.assetTypeSelections.push({'selection': 'income_producing'});
            } else if (option.value == 'private_seller') {
                $scope.assetTypeSelections.push({'selection': 'private_seller'});
            } else if (option.value == 'short_sell') {
                $scope.assetTypeSelections.push({'selection': 'short_sell'});
            }
        };

        $scope.addRunNumberOption = function(option) {
            if (option.value == 'less_than') {
                $scope.runNumberSelections.push({'selection': 'less_than'});
            } else if (option.value == 'greater_than') {
                $scope.runNumberSelections.push({'selection': 'greater_than'});
            } else if (option.value == 'between') {
                $scope.runNumberSelections.push({'selection': 'between'});
            }
        };

        $scope.addSellerCodeOption = function(option) {
            if (option.value == '211') {
                $scope.sellerCodeSelections.push({'selection': '211'});
            } else if (option.value == '3ml') {
                $scope.sellerCodeSelections.push({'selection': '3ml'});
            } else if (option.value == '400') {
                $scope.sellerCodeSelections.push({'selection': '400'});
            } else if (option.value == '505') {
                $scope.sellerCodeSelections.push({'selection': '505'});
            } else if (option.value == '639') {
                $scope.sellerCodeSelections.push({'selection': '639'});
            } else if (option.value == 'aaa') {
                $scope.sellerCodeSelections.push({'selection': 'aaa'});
            }
        };

        $scope.addStatusTypeOption = function(option) {
            if (option.value == 'auction') {
                $scope.statusSelections.push({'selection': 'auction'});
            } else if (option.value == 'canceled') {
                $scope.statusSelections.push({'selection': 'canceled'});
            } else if (option.value == 'postponed') {
                $scope.statusSelections.push({'selection': 'postponed'});
            } else if (option.value == 'sold_presale') {
                $scope.statusSelections.push({'selection': 'sold_presale'});
            }
        };

        $scope.addDaysToAuctionOption = function(option) {
            if (option.value == 'less_than') {
                $scope.daysToAuctionSelections.push({'selection': 'less_than'});
            } else if (option.value == 'greater_than') {
                $scope.daysToAuctionSelections.push({'selection': 'greater_than'});
            } else if (option.value == 'between') {
                $scope.daysToAuctionSelections.push({'selection': 'between'});
            }
        };

        $scope.addStartingBidOption = function(option) {
            if (option.value == 'less_than') {
                $scope.startingBidSelections.push({'selection': 'less_than'});
            } else if (option.value == 'greater_than') {
                $scope.startingBidSelections.push({'selection': 'greater_than'});
            } else if (option.value == 'between') {
                $scope.startingBidSelections.push({'selection': 'between'});
            }
        };

        $scope.addZoltarScoreOption = function(option) {
            if (option.value == 'less_than') {
                $scope.zoltarScoreSelections.push({'selection': 'less_than'});
            } else if (option.value == 'greater_than') {
                $scope.zoltarScoreSelections.push({'selection': 'greater_than'});
            } else if (option.value == 'between') {
                $scope.zoltarScoreSelections.push({'selection': 'between'});
            }
        };

        $scope.goToResults = function() {
            console.log("inside here");
            $timeout(function() {
                $location.hash("result-container");
                // call $anchorScroll()
                $anchorScroll();
            });
        };

        $scope.searchProperties = function() {
            $scope.markers = [];
            $scope.search_initiated = true;
            console.log($scope.search_initiated);
            console.log("Inside searchProperties");
            $scope.map = { center: { latitude: $scope.selected.lat, longitude: $scope.selected.long }, zoom: 7 };
            console.log("boostBy " + boostBy);
            if ($scope.useExistingRuleset == true) {
                Solstice.search({ 
                     q: $scope.selected.abbr,
                     bq: boostBy,
                     rows: 10 
                }) 
                .then(function (data){ 
                    console.log(data);
                    console.log(data.data.response.docs);
                    console.log(data.data.response.docs.length);
                    $scope.resultCount = data.data.response.docs.length;
                    for (var i =0; i < data.data.response.docs.length; i++) {
                        // console.log(data.data.response.docs[i].Address[0]);
                        console.log(data.data.response.docs[i].City[0]);
                        var location = data.data.response.docs[i].Address[0];
                        location+=" ";
                        location+=data.data.response.docs[i].City[0];
                        if (!this.geocoder) this.geocoder = new google.maps.Geocoder();
                        this.geocoder.geocode({ 'address': location }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var loc = results[0].geometry.location;
                                $scope.markers.push({
                                    id: i,
                                    latitude: loc.lat(),
                                    longitude: loc.lng(),
                                    showWindow: false,
                                });
                                //$scope.search = results[0].formatted_address;
                                //$scope.gotoLocation(loc.lat(), loc.lng());
                            } else {
                                // alert("Sorry, this search produced no results.");
                            }
                        });
                    }
                    $scope.propertyList = data.data.response.docs;
                    boostBy="";
                    // data.forEach(function (d) {
                    //     console.log(d);
                    // });
                });
            } else { // useExistingRuleset == false ; ruleset is not saved

            }
        };
    }
]);