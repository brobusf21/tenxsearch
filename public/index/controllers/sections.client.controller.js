// Create the 'sections' controller
angular.module('index').controller('SectionController', ['$scope', '$routeParams', '$location', 'Rulesets',
    function($scope, $routeParams, $location, Rulesets, ExistingRulesets) {

        $scope.section1 = false;
        $scope.section2 = false;
        $scope.section3 = false;
        $scope.section4 = false;
        $scope.section5 = false;
        $scope.search_initiated = false;


        $scope.selectSectionParent = function(setSection) {
            if (setSection == 1) {
                if ($scope.section1 == true) {
                    // console.log("ALREADY OPENED");
                    hideSection(1);
                } else {
                    // console.log("Section 1");
                    showSection(1);
                }
            } else if (setSection == 2) {
                // console.log("Section 2");
                if ($scope.section2 == true) {
                    // console.log("ALREADY OPENED");
                    hideSection(2);
                } else {
                    // console.log("Section 2");
                    showSection(2);
                }
            } else if (setSection == 3) {
                // console.log("Section 3");
                if ($scope.section3 == true) {
                    // console.log("ALREADY OPENED");
                    hideSection(3);
                } else {
                    // console.log("Section 3");
                    showSection(3);
                }
            } else if (setSection == 4) {
                // console.log("Section 4");
                if ($scope.section4 == true) {
                    // console.log("ALREADY OPENED");
                    //hideSection(4);
                } else {
                    // console.log("Section 4");
                    showSection(4);
                }
            } else if (setSection == 5) {
                if ($scope.section5 == true) {
                    // console.log("ALREADY OPENED");
                    hideSection(5);
                } else {
                    showSection(5);
                }
            }
        };

        $scope.isSelectedParent = function(checkSection) {
            //return $scope.section == checkSection;
            if (checkSection == 1) {
                // console.log("isSelectedParent " + checkSection + " " + $scope.section1);
                return $scope.section1;
            } else if (checkSection == 2) {
                // console.log("isSelectedParent " + checkSection + " " + $scope.section2);
                return $scope.section2;
            } else if (checkSection == 3) {
                // console.log("isSelectedParent " + checkSection + " " + $scope.section3);
                return $scope.section3;
            } else if (checkSection == 4) {
                // console.log("isSelectedParent " + checkSection + " " + $scope.section4);
                return $scope.section4;
            } else if (checkSection == 5) {
                // console.log("isSelectedParent " + checkSection + " " + $scope.section4);
                return $scope.section5;
            }
        };

        $scope.closeAll = function() {
            $scope.section1 = false;
            $scope.section2 = false;
            $scope.section3 = false;
            $scope.section4 = false;
            $scope.section5 = false;
        };

        function hideSection(sec) {
            // console.log("hideSection");
            if (sec == 1) {
                $scope.section1 = false;
                $scope.section2 = false;
                $scope.section3 = false;
            } else if (sec == 2) {
                $scope.section2 = false;
            } else if (sec == 3) {
                $scope.section3 = false;
            } else if (sec == 4) {
                $scope.section4 = false;
            } else if (sec == 5) {
                $scope.section5 = false;
            }

        };

        function showSection(sec) {
            if (sec == 1) {
                $scope.section1 = true;
            } else if (sec == 2) {
                $scope.section2 = true;
                $scope.section3 = false;
            } else if (sec == 3) {
                $scope.section3 = true;
            } else if (sec == 4) {
                $scope.section4 = true;
            } else if (sec == 5) {
                $scope.section5 = true;
            }
        };

        $scope.changeSearch = function() {
            $scope.search_initiated = true;
            console.log($scope.search_initiated);
        };
    }
]);