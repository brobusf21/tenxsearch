// Invoke 'strict' JavaScript mode
'use strict';

// Create the 'index' module
angular.module('index', ['ui.bootstrap', 'solstice', 'ui.bootstrap.typeahead', 'uiGmapgoogle-maps']);