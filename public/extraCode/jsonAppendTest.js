var data = [
	{
		"Auction ID":"B849-CY200",
		"Property Type":"Office",
		"Address":"7389 And 7389 Rosecrans Ave.",
		"City":"Manhattan Beach",
		"State":"CA",
		"Zip":"90266",
		"Sold (Yes/No)":"No",
		"Closed (Yes/No)":"No",
		"Registered Bidders":17,
		"High Bid":"$110,438,840.00",
		" Reserve ":"$127,321,320.00",
		"Total Price w/BP":"$48,075,734.00",
		" Value (High) ":"$152,785,584.00",
		" Value (Low) ":"$101,857,056.00",
		"Valuation Analyst":"Derrick Gottlieb"
	}
];

console.log(data.length);