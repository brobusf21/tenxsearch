// Invoke 'strict' JavaScript mode
'use strict';

// Set the main application name
var mainApplicationModuleName = 'tenxsearch';

// Create the main application
var mainApplicationModule = angular.module(mainApplicationModuleName, ['ngResource', 'ngRoute', 'solstice', 'index']);

// Configure the hashbang URLs using the $locationProvider services 
mainApplicationModule.config(['$locationProvider', 'SolsticeProvider',
	function($locationProvider, SolsticeProvider) {
		$locationProvider.hashPrefix('!');
		SolsticeProvider.setEndpoint('http://localhost:8983/solr/tenxsearch');
	}
]);

// Configure Solstice to connect to SOLR instance
// mainApplicationModule.config(function(SolsticeProvider) {
//   SolsticeProvider.setEndpoint('http://localhost:8983/solr/tenxsearch');
// });

// Manually bootstrap the AngularJS application
angular.element(document).ready(function() {
	angular.bootstrap(document, [mainApplicationModuleName]);
});