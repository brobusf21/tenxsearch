/* Load the module dependencies */
var express = require('express'),
	morgan = require('morgan'),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override'),
	config = ('./config'),
	session = require('express-session');

module.exports = function() {
	/* Create a new Express application instance */
	var app = express();

	/* Use the 'body-parser' and 'method-override' middleware functions */
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	app.use(methodOverride());

	// Might not need sessions --> Thats why it is commented out
	/* Configure the 'session' middleware */
	// app.use(session({
	// 	saveUninitialized: true,
	// 	resave: true,
	// 	secret: config.sessionSecret
	// }));

	/* Set the application view engine and 'views' folder */
	app.set('views', './app/views');
	app.set('view engine', 'ejs');

	/* Load the routing files */
	require('../app/routes/index.server.routes.js')(app);
	require('../app/routes/rulesets.server.routes.js')(app);
	/* Configure static file serving */
	app.use(express.static('./public'));

	return app;
};