/* Load the module dependencies */
var mongoose = require('mongoose');
	uri = 'mongodb://brandon:usfca21@ds019048.mlab.com:19048/tenxsearch';

/* Define the Mongoose configuration method */
module.exports = function() {

	/* Use Mongoose to connect to MongoDB */
	var db = mongoose.connect(uri);

	/* If connection throws an error*/
	mongoose.connection.on('error', function(err) {
		console.log('Mongoose default connection error: ' + err);
	});

	/* When the connection gets disconnected */
	mongoose.connection.on('disconnected', function(err) {
		console.log('Mongoose default connection disconnected');
	});

	// Load the ruleset model 
	require('../app/models/ruleset.server.model');

	return db;
};