# Purpose #
This program was written and presented as a POC for Auction.com employees back in 2016 as potential internship project.

Our final version was given to Auction.com at the end of our internship; however, the overall functionality was similiar in the sense we used the MEAN stack
(Node.js, Express.js, MongoDB, and AngularJS). Our application allowed Auction.com employees to create Apache Solr rulesets to query their database for more
efficient search results.

# Configuration #
Once the files are downloaded, you can run the application from the root directory in the command line by entering node server.