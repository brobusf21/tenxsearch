/* Load the Mongoose module and Schema object */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

// Temporary Schema
var RulesetSchema = new Schema({
	title: {
		type: String
	},
	created: {
		type: Date,
		default: Date.now
	},
	rulesetArray: []
});

mongoose.model('Ruleset', RulesetSchema);


	// asset_type: [],
	// run_number: [],
	// seller_code: [],
	// status: [],
	// days_to_auction: [],
	// starting_bid: [],
	// zoltar_score: [],


	// run_number: {
	// 	less_than: {
	// 		type: Number
	// 	},
	// 	greater_than: {
	// 		type: Number
	// 	},
	// 	between1: {
	// 		type: Number
	// 	},
	// 	between2: {
	// 		type: Number
	// 	}
	// },
	// seller_code: {
	// 	type: String
	// },
	// status: {

	// },
	// zoltar_score: {

	// },
	// starting_bid: {

	// },
	// days_to_auction: {

	// },
	// created: {
	// 	type: Date,
	// 	default: Date.now
	// }