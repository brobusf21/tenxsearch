// Load the module dependencies
var rulesets = require('../../app/controllers/ruleset.server.controller');

// Define the routes module' method
module.exports = function(app) {
	// Set up the 'rulesets' base routes 
	app.route('/api/rulesets')
		.get(rulesets.list)
	    .post(rulesets.create);

	app.route('/api/rulesets/:rulesetId')
		.get(rulesets.read)
		.put(rulesets.update)
		.delete(rulesets.delete);
		
	// Set up the 'rulesetId' parameter middleware   
	app.param('rulesetId', rulesets.rulesetByID);
};