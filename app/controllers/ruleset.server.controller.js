/* Load the module dependencies */
var mongoose = require('mongoose'),
	Ruleset = mongoose.model('Ruleset');
	//solr = require('solr-client');
	//client = solr.CreateClient(); 

/* Function:    getErrorMessage
 * Description: Error handling controller method
 * Input:       err
 */
var getErrorMessage = function(err) {
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) return err.errors[errName].message;
		}
	} else {
		return 'Unknown server error';
	}
};

/* Function:    create
 * Description: Controller method that creates new rule sets and saves them in our MongoDB
 * Input:       req, res
 */
exports.create = function(req, res) {

	// Create a new ruleset object
	console.log("req.body: " + req.body);
	var ruleset = new Ruleset(req.body);
	console.log("ruleset: " + ruleset);

	// Try saving the ruleset document to the database
	ruleset.save(function(err) {
		if (err) {
			// If an error occurs send the error message
			console.log("Error");
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the article
			console.log("No error!");
			// This is not necessary but we would send it back if we wanted to use an attribute of it
			res.json(ruleset);
		}
	});
};


// Create a new controller method that retrieves a list of rulesets
exports.list = function(req, res) {
	// Use the model 'find' method to get a list of articles
	Ruleset.find().exec(function(err, rulesets) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the article 
			res.json(rulesets);
		}
	});
};

// Create a new controller method that returns an existing ruleset
exports.read = function(req, res) {
	console.log("Entering exports.read");
	console.log("Get request");
	res.json(req.ruleset);
};


// Create a new controller middleware that retrieves a single existing ruleset
exports.rulesetByID = function(req, res, next, id) {
	// Use the model 'findById' method to find a single ruleset 
	Ruleset.findById(id).exec(function(err, ruleset) {
		if (err) return next(err);
		if (!ruleset) return next(new Error('Failed to load ruleset ' + id));

		// If a ruleset is found use the 'request' object to pass it to the next middleware
		req.ruleset = ruleset;

		// Call the next middleware
		next();
	});
};

// Create a new controller method that deletes a ruleset
exports.delete = function(req, res) {
	// Get the ruleset from the 'request' object
	var ruleset = req.ruleset;

	// Use the model 'remove' method to delete the ruleset
	ruleset.remove(function(err) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the ruleset
			res.json(ruleset);
		}
	});
};

// Create a new controller method that updates an existing ruleset
exports.update = function(req, res) {
	// Get the article from the 'request' object
	var ruleset = req.ruleset;

	// Update the article fields
	console.log("Inside of server update: " + ruleset);
	console.log("New title: " + req.body.title);
	console.log("Added selections are....to be continued");
	ruleset.title = req.body.title;
	ruleset.rulesetSelections = req.body.rulesetSelections;
	// Try saving the ruleset document to the database
	ruleset.save(function(err) {
		if (err) {
			// If an error occurs send the error message
			console.log("Error");
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the article
			console.log("No error!");
			// This is not necessary but we would send it back if we wanted to use an attribute of it
			res.json(ruleset);
		}
	});
};